==========
 About Me
==========

:title: About Me

I love to make things, no matter the medium. I work as a software engineer where
I create software, but I also enjoy DIY projects around the house and creating
things with my kids.  From woodworking, to crafting, and yes even a bit of
sewing, it is all fair game!

.. image:: /images/KidsBlocks.jpg
   :align: center
   :alt: Curtis Kids

