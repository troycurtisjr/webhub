=================================
 Free/Libre Open Source Software
=================================

:title: FLOSS

My first experience with Free/Libre Open Source Software (FLOSS) was with
Mandrake Linux, given to me on a set of disks by a High School friend. Over the
years I've distro-hopped through a lot of different Linux variants, finally
settling on `Fedora <https://getfedora.org>`_. RPM-based distros just feel very
comfortable to me, and I used RHEL/CentOS in my day job for nearly 13 years, so
there was a lot of "cross pollination". Around 2005, I switched to using Linux
full-time on all my home systems. I've also had a few dual boot machines here and
there, but I rarely find a reason to boot into Windows.

I work as a software developer, and I am fortunate to build all of my projects
on top FLOSS applications. For years I had to use a Windows system for various
corporate items, such as email and a scattering of process tools, but at my new
job it is all on MacOS. This took some getting used to, but I really appreciate
how much easier it is to pull in so many familiar Linux tools. This is really
great, as I chose a software engineering focus mostly because of my increasing
interest in Linux and the communities around Free Software while I was working
toward my Electrical Engineering degree.

In 2008, I worked with the `Subversion <https://subversion.apache.org>`_
developers to implement a `new feature
<http://www.red-bean.com/svnproject/contribulyzer/detail/troycurtisjr%40gmail.html>`_.
The Subversion guys are really excellent, with high standards and attention to
detail, while being very welcoming and supportive. Working through this project
was one of the most rewarding and influential software development experiences I
have been involved in. As a result of that experience, I have also suggested to
every Co-Op I have mentored to find a project to contribute to in order to
really improve their skills.

.. image:: /images/Logo_fedoralogo.png
   :width: 200px
   :align: left
   :alt: Fedora
   :target: https://getfedora.org

Shortly after this Subversion feature implementation, I stopped having as much
time to contribute. My young family started to get very busy, and my day job got
more demanding. However, I never stopped tracking new developments in the FLOSS
community and learning new technologies. Almost a decade later, my kids were
getting older and I found time to once again participate. As I have been
using more Python, I decided to start helping out `Fedora's Python 3 Porting
Effort <http://fedora.portingdb.xyz/>`_. It was awesome learning about how
the distro is actually put together and how much it takes to make it successful.

.. image:: /images/svn-banner-combo.jpg
  :width: 200px
  :align: right
  :alt: Subversion
  :target: https://subversion.apache.org/
        
While looking through the list of packages that did not yet support Python 3, I
ran across a familiar project: Subversion! It seemed like a perfect re-entrance
into contribution by working with the awesome Subversion devs once again! With
the help of the great contributors at Subversion I did get through a lot of
initial work for getting the Subversion bindings over to Python 3. Alas, once
again life got in the way, moving across the country and starting a new job took
over most of my free time. Additionally, at the new job I no longer used
Subversion in any capacity, which further distracted my attention to other
priorities. After I let the branch lapse for a while, Yasuhito FUTATSUKI took
interest and started it back up. Eventually, he worked with the rest of
committers and managed to get it over the finish line and Subversion 1.14
released with Python 3 support.

Fast forward a couple of years, and I've continued to do little things on
occasion with Fedora. I've been on the look out for a package to take over that
I am interested in, but nothing has come up at this point. Other than the bug
report here and there and some update testing, I did write a few articles for
`Fedora Magazine <https://fedoramagazine.org/author/troycurtisjr/>`_. I also
worked a bit with `Ulauncher <https://github.com/Ulauncher/Ulauncher>`_ and I
continue to play around with GNU Radio, including taking over the `gr-pager
<https://github.com/troycurtisjr/gr-pager>`_ module. Mostly, my contributions
are just here and there as I have interest and time.
