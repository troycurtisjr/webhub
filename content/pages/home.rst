Welcome to the Web Hub of Troy Curtis, Jr.
==========================================

:save_as: index.html
:status: hidden

Welcome to the Web Hub for Troy Curtis, Jr. For now, this is just a place
pointing to all the various internet things I'm involved in, and any other
random bits I want to put out.  Perhaps one day I'll write an article or two!

Most things will be related to kids, Linux, FLOSS, Making, or hiking.

.. image:: /images/PXL_20201126_174744570.jpg
   :align: center
   :alt: Troy and kids at Seagrove's Farm Park
