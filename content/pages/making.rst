================
 Aspiring Maker
================

:title: Aspiring Maker

I love to learn how things work, to make new things, and to fix rather than
replace.  Figuring out some new skill for a project is such a great way to enjoy
life! Every time I can find an excuse to put something together, I take it.  It
isn't always pretty, but it is always fun. Even as a kid I would take things
apart to see how they worked, which is why I chose to be an engineer. As much as
I love science and math, the *application* of those techniques is where it is
at.

.. lightbox::
   :thumb: /images/SchoolCardboardArcade-sm.jpg
   :large: /images/SchoolCardboardArcade.jpg
   :align: left
   :alt: School Cardboard Arcade

I am happy to see that the Maker Movement has been gaining steam. Even though
shop classes are getting canceled all over the state, now there are at least
Make inspired school activities. One of the larger such events at my kids' school is
The Cardboard Arcade inspired by `Caine's Arcade <http://cainesarcade.com/>`_.
My kids had so much fun they even built their own here at home, more than once!

Mostly I just tinker around, and don't have a huge list of impressive projects,
but I enjoy doing them. Here is a bit about a couple of my favorites.

Tree Stump
==========

.. lightbox::
   :thumb: /images/StumpInProgress-sm.png
   :large: /images/StumpInProgress.png
   :align: right
   :alt: Stump In Progress

So far the project I am most proud of is a tree stump prop I built for my
daughter's competition dance solo for 2016/2017. A perfect combination of
woodworking and art. It had to be strong because my daughter would be jumping on
and off of it, but light enough that my wife could easily handle it at events.
Plus, it had to look great! I got the skeleton together fairly quickly so that
she could practice it with it. I used 3/4 inch plywood for strength, and then
cut out large sections of the interior panels to help reduce weight. Then I went
through many trials with cardboard, Great Stuff, and
wood-glue-soaked-paper-towel trying to get the texture right. I finally finished
it up over Christmas break in time for competition season.

During competitions, the prop worked out perfectly.  There were lots of compliments from
other parents and dance studios, a judge even called it out! At one competition
a mom tried her best to get my wife to sell it so she could put it in her
daughter's room 😄. As proud as I was of the prop I made, it still just served
as a backdrop to my girl's fantastic dancing!

.. image:: /images/StumpOnStage.png
   :name: stumponstage
   :width: 300px
   :align: center
   :alt: Stump On Stage

DIY
===

.. lightbox::
   :thumb: /images/microwave-sm.jpg
   :large: /images/microwave.jpg
   :align: right
   :alt: Microwave

Along with making new things I strive to "Do It Yourself" whenever possible.
Wall repair, plumbing, car maintenance, and whatever else comes along. I like it
the best when I get to learn some new skill that I've never done before. It
probably takes longer, and in some cases it might not be as perfect as a
professional. However, I always enjoy it and it still gets done. Though there are
a few exceptions. I don't change my own oil since it is a messy, boring chore
that also doesn't save much money. Also recently I had my garage door spring
changed out (mostly because the guy had the spring in stock on his truck *that
day*).

.. lightbox::
   :thumb: /images/Backdoor-sm.jpg
   :large: /images/Backdoor.jpg
   :align: left
   :alt: Backdoor

Owning a home certainly provides plenty of opportunity to scratch my DIY itch
and sometimes mini-disasters turn into opportunities. One day I wasn't paying
enough attention and my Weed Eater threw a rock into the glass of my back door!
It was immediately cracked all over, and I could literally hear it cracking more
as I inspected it. So I quickly taped up some plastic to contain any falling
glass shards and headed to Lowe's where we picked up one of those nifty doors
with the blinds embedded in-between the panes of glass. Certainly much nicer
than what we had (even before the cracked glass). We even managed to get the
door in the back of our Toyota Highlander. A few YouTube videos later and I was
ripping out trim and the old door, and putting the new one in place. It
certainly was not what I had in mind for that weekend, but now there are no more
dogs messing up the blinds, and I could add another notch to my DIY belt.
