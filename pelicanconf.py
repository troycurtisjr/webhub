# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Troy Curtis, Jr.'
SITENAME = 'The Web Hub of Troy Curtis, Jr.'
SITETITLE = 'Troy Curtis, Jr'
SITESUBTITLE = 'Father. Software Engineer. Aspiring Maker.'
SITEURL = 'http://localhost:8000'
FAVICON = "/images/dragon_fang.jpg"

AVATAR_AUTHOR_EMAIL = 'troy@troycurtisjr.com'
AVATAR_SIZE = 150

COPYRIGHT_YEAR = 2021

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'slug': 'by-sa',
    'version': '4.0'
}

PATH = 'content'

TIMEZONE = 'US/Eastern'

DEFAULT_LANG = 'en'

# Typically need to set to False when tweaking settings
#LOAD_CONTENT_CACHE = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

MAIN_MENU = False

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

LINKS = ()

# Social widget
SOCIAL = (
    ('linkedin', 'https://www.linkedin.com/in/troycurtisjr/'),
    ('gitlab', 'https://gitlab.com/troycurtisjr'),
    ('github', 'https://github.com/troycurtisjr'),
    ('stack-overflow', 'https://stackoverflow.com/users/8509369/troycurtisjr'),
    ('facebook', 'https://www.facebook.com/troycurtisjr'),
    ('envelope', 'mailto:troy@troycurtisjr.com'),
)

CUSTOM_SOCIAL_LINKS = (
    '<a class="sc-fedora" href="https://src.fedoraproject.org/user/troycurtisjr" target="_blank"><img class="fa" src="/images/Fedora_infinity.png" /></a>',
    '<a href="https://keybase.io/troycurtisjr" target="_blank"><img src="/images/icon-keybase-logo-48.png" /></a>',
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = './themes/pelican-flex-solar'

STATIC_PATHS = [ 'images', 'css' ]

CUSTOM_CSS = 'css/webhub.css'

PLUGIN_PATHS = [ 'plugins/', ]

PLUGINS = [
    'lightbox',
    'avatar'
]

